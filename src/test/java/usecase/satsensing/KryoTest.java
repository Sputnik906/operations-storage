package usecase.satsensing;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import de.javakaffee.kryoserializers.jodatime.JodaIntervalSerializer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.junit.jupiter.api.Test;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;
import usecase.satsensing.domain.ShootWorkOption;
import usecase.satsensing.plandomain.ISatsensingScheduleFacade;
import usecase.satsensing.plandomain.SatsensingScheduleFactory;

public class KryoTest {

  @Test
  public void kryoSatsensingScheduleTest() {

    Kryo kryo = new Kryo();
    kryo.register(Interval.class, new JodaIntervalSerializer());
    kryo.setRegistrationRequired(false);
    kryo.setReferences(true);

    ISatsensingScheduleFacade satsensingScheduleFacade =
        SatsensingScheduleFactory.syncWorkConstraintUniqueShootWork();

    Instant baseTime = Instant.parse("2019-01-01T00:00:00Z");

    ShootPointObjectTask task = new ShootPointObjectTask("Task 1");

    Satellite satellite = new Satellite("Satellite 1", 1000);

    Interval shootInterval =
        new Interval(baseTime.getMillis(), baseTime.plus(Duration.standardSeconds(10)).getMillis());

    GroundStation groundStation = new GroundStation("Ground station 1");

    Interval dropInterval =
        new Interval(
            baseTime.plus(Duration.standardSeconds(20)).getMillis(),
            baseTime.plus(Duration.standardSeconds(40)).getMillis());

    ShootWorkOption shootWorkOption =
        new ShootWorkOption(task, satellite, shootInterval, groundStation, dropInterval, 1000);

    satsensingScheduleFacade.addSatellite(shootWorkOption.getSatellite());
    satsensingScheduleFacade.addGroundStation(shootWorkOption.getGroundStation());
    satsensingScheduleFacade.addShootWorks(
        Collections.singletonList(shootWorkOption.toShootWorkHoldAllVisible()));

    Set<ShootWork> shootWorks = satsensingScheduleFacade.getAllShootWorks();

    Output output = new Output(1024, -1);

    kryo.writeObject(output, shootWorks);
    output.close();

    Input input = new Input(output.getBuffer(), 0, output.position());
    Set<ShootWork> deserShootWorks = kryo.readObject(input, HashSet.class);
    input.close();

    ISatsensingScheduleFacade deserSatsensingScheduleFacade =
        SatsensingScheduleFactory.syncWorkConstraintUniqueShootWork();
    deserShootWorks.forEach(
        sw -> {
          deserSatsensingScheduleFacade.addSatellite(sw.getShootWorkOption().getSatellite());
          deserSatsensingScheduleFacade.addGroundStation(
              sw.getShootWorkOption().getGroundStation());
        });
    deserSatsensingScheduleFacade.addShootWorks(new ArrayList<>(deserShootWorks));

    System.out.println(deserSatsensingScheduleFacade.getAllShootWorks());
  }
}
