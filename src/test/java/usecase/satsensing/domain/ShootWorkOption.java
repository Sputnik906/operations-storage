package usecase.satsensing.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.joda.time.Interval;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@ToString
@EqualsAndHashCode
@Getter
public class ShootWorkOption {
  @NonNull private final ShootPointObjectTask task;
  @NonNull private final Satellite satellite;
  @NonNull private final Interval shootVisible;
  @NonNull private final GroundStation groundStation;
  @NonNull private final Interval dropVisible;

  private final long imageValue;

  public ShootWorkOption with(ShootPointObjectTask task) {
    return new ShootWorkOption(
        task, satellite, shootVisible, groundStation, dropVisible, imageValue);
  }

  public ShootWork toShootWorkHoldAllVisible() {
    return new ShootWork(this, shootVisible, dropVisible);
  }
}
