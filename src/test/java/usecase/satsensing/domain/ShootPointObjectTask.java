package usecase.satsensing.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@ToString
@Getter
public class ShootPointObjectTask {
  @NonNull private final String label;
}
