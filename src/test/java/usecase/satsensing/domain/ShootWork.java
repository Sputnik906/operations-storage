package usecase.satsensing.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.joda.time.Interval;

@ToString
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@Getter
public class ShootWork {

  private final ShootWorkOption shootWorkOption;

  private final Interval shootInterval;

  private final Interval dropInterval;

  public ShootWork(ShootWorkOption shootWorkOption, Interval shootInterval, Interval dropInterval) {
    if (shootWorkOption == null) throw new IllegalArgumentException("shootWorkOption");
    if (dropInterval.getStartMillis() < shootInterval.getEndMillis())
      throw new IllegalArgumentException("dropInterval is early then shootInterval");
    this.shootWorkOption = shootWorkOption;
    this.shootInterval = shootInterval;
    this.dropInterval = dropInterval;
  }

  public ShootWork with(ShootPointObjectTask task) {
    return new ShootWork(shootWorkOption.with(task), shootInterval, dropInterval);
  }
}
