package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IVolumeResource;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.VolumeOperation;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume.KeepVolumeOperation;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joda.time.Interval;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

public interface ISatsensingScheduleFacade {
  boolean addGroundStation(GroundStation groundStation);

  boolean addGroundStations(Collection<GroundStation> groundStations);

  Set<GroundStation> getGroundStations();

  Set<Satellite> getSatellites();

  boolean addSatellite(Satellite satellite);

  boolean addSatellites(Collection<Satellite> satellites);

  Optional<Set<ShootWork>> addGroundStationConstraint(
      GroundStation groundStation, Interval interval);

  Optional<Set<ShootWork>> addSatelliteConstraint(Satellite satellite, Interval interval);

  boolean removeGroundStationConstraint(GroundStation groundStation, Interval interval);

  boolean removeSatelliteConstraint(Satellite satellite, Interval interval);

  Set<WorkConstraint<GroundStation>> getGroundStationConstraints(
      GroundStation groundStation, Interval interval);

  Set<WorkConstraint<Satellite>> getSatelliteConstraints(Satellite satellite, Interval interval);

  ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks);

  ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks);

  Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(Satellite satellite);

  Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite, Interval intersectedInterval);

  Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation);

  Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation, Interval intersectedInterval);

  Optional<Set<ShootWork>> getShootWorksByGroundStation(
      GroundStation groundStation, Interval interval);

  Optional<Set<ShootWork>> getShootWorksBySatellite(Satellite satellite, Interval interval);

  Optional<Set<ShootWork>> removeSatellite(Satellite satellite);

  Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation);

  boolean removeShootWork(ShootWork shootWork);

  Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks);

  ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands);

  ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands);

  Optional<ShootWork> getShootWork(ShootPointObjectTask task);

  boolean containsShootWork(ShootWork shootWork);

  Set<ShootWork> getAllShootWorks();

  default List<IOperation<?>> operationsFrom(ShootWork shootWork) {
    return Arrays.asList(
        new ShootOperation(shootWork),
        new DownloadOperation(shootWork),
        new KeepImageInSatelliteOperation(shootWork),
        new UploadOperation(shootWork));
  }

  class ShootOperation extends OperationWithShootWork<Satellite> {
    ShootOperation(ShootWork shootWork) {
      super(shootWork, shootWork.getShootWorkOption().getSatellite(), shootWork.getShootInterval());
    }
  }

  class DownloadOperation extends OperationWithShootWork<Satellite> {
    DownloadOperation(ShootWork shootWork) {
      super(shootWork, shootWork.getShootWorkOption().getSatellite(), shootWork.getDropInterval());
    }
  }

  class UploadOperation extends OperationWithShootWork<GroundStation> {
    UploadOperation(ShootWork shootWork) {
      super(
          shootWork,
          shootWork.getShootWorkOption().getGroundStation(),
          shootWork.getDropInterval());
    }
  }

  class KeepImageInSatelliteOperation extends KeepVolumeOperation<SatelliteMemory> {

    KeepImageInSatelliteOperation(ShootWork shootWork) {
      super(new IncreaseMemoryOperation(shootWork), new ReducingMemoryOperation(shootWork));
    }
  }

  class IncreaseMemoryOperation extends VolumeOperation<SatelliteMemory> {

    IncreaseMemoryOperation(ShootWork shootWork) {
      super(
          new SatelliteMemory(shootWork.getShootWorkOption().getSatellite()),
          shootWork.getShootInterval(),
          shootWork.getShootWorkOption().getImageValue());
    }
  }

  class ReducingMemoryOperation extends VolumeOperation<SatelliteMemory> {

    ReducingMemoryOperation(ShootWork shootWork) {
      super(
          new SatelliteMemory(shootWork.getShootWorkOption().getSatellite()),
          shootWork.getDropInterval(),
          -shootWork.getShootWorkOption().getImageValue());
    }
  }

  @Getter
  @RequiredArgsConstructor
  @EqualsAndHashCode
  class SatelliteMemory implements IVolumeResource {

    @NonNull private final Satellite satellite;

    @Override
    public long minValue() {
      return 0;
    }

    @Override
    public long maxValue() {
      return satellite.getMemory();
    }
  }
}
