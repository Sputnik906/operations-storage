package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import com.gitlab.sputnik906.operationsstorage.operationtreeimpl.intervaltree.LongInterval;
import com.lodborg.intervaltree.IntervalTree;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;
import org.joda.time.Interval;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootWork;

public class ResourceWorkConstraintDelegate extends AbstractSatsensingScheduleDelegate {
  private final Map<Object, IntervalTree<Long>> resourceConstraintMap;

  public ResourceWorkConstraintDelegate(
      ISatsensingScheduleFacade delegate, Map<Object, IntervalTree<Long>> resourceConstraintMap) {
    super(delegate);
    this.resourceConstraintMap = resourceConstraintMap;
  }

  private Optional<Set<ShootWork>> getShootWorksByResource(Object resource, Interval interval) {
    if (resource instanceof Satellite)
      return getShootWorksBySatellite((Satellite) resource, interval);
    if (resource instanceof GroundStation)
      return getShootWorksByGroundStation((GroundStation) resource, interval);
    throw new IllegalStateException();
  }

  private Optional<Set<ShootWork>> addConstraint(WorkConstraint<Object> workConstraint) {
    IntervalTree<Long> constraints =
        resourceConstraintMap.computeIfAbsent(
            workConstraint.getResource(), k -> new IntervalTree<>());
    if (constraints.add(new WorkConstraintToIntervalAdapter<>(workConstraint)))
      return getShootWorksByResource(workConstraint.getResource(), workConstraint.getInterval())
          .map(s -> s.stream().peek(this::removeShootWork).collect(Collectors.toSet()));
    return Optional.of(new HashSet<>());
  }

  @Override
  public Optional<Set<ShootWork>> addGroundStationConstraint(
      GroundStation groundStation, Interval interval) {
    return addConstraint(new WorkConstraint<>(groundStation, interval));
  }

  @Override
  public Optional<Set<ShootWork>> addSatelliteConstraint(Satellite satellite, Interval interval) {
    return addConstraint(new WorkConstraint<>(satellite, interval));
  }

  private boolean removeConstraint(WorkConstraint<Object> workConstraint) {
    return resourceConstraintMap
        .getOrDefault(workConstraint.getResource(), new IntervalTree<>())
        .remove(new WorkConstraintToIntervalAdapter<>(workConstraint));
  }

  @Override
  public boolean removeGroundStationConstraint(GroundStation groundStation, Interval interval) {
    return removeConstraint(new WorkConstraint<>(groundStation, interval));
  }

  @Override
  public boolean removeSatelliteConstraint(Satellite satellite, Interval interval) {
    return removeConstraint(new WorkConstraint<>(satellite, interval));
  }

  private <ResourceT> Set<WorkConstraint<ResourceT>> getConstraints(
      ResourceT object, Interval interval) {
    return resourceConstraintMap.getOrDefault(object, new IntervalTree<>())
        .query(LongInterval.from(interval)).stream()
        .map(o -> ((WorkConstraintToIntervalAdapter<ResourceT>) o).workConstraint)
        .collect(Collectors.toSet());
  }

  @Override
  public Set<WorkConstraint<GroundStation>> getGroundStationConstraints(
      GroundStation groundStation, Interval interval) {
    return getConstraints(groundStation, interval);
  }

  @Override
  public Set<WorkConstraint<Satellite>> getSatelliteConstraints(
      Satellite satellite, Interval interval) {
    return getConstraints(satellite, interval);
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    return addShootWorksWithCheckConstraint(false, shootWorks, new HashSet<>());
  }

  @Override
  public ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    return addShootWorksWithCheckConstraint(true, shootWorks, ignoryShootWorks);
  }

  private ResultAddShootWorks addShootWorksWithCheckConstraint(
      boolean test, List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    Set<WorkConstraint<?>> constraints =
        shootWorks.stream()
            .flatMap(shootWork -> operationsFrom(shootWork).stream())
            .filter(
                o ->
                    (o.getResource() instanceof Satellite)
                        || (o.getResource() instanceof GroundStation))
            .flatMap(
                o ->
                    resourceConstraintMap.getOrDefault(o.getResource(), new IntervalTree<>())
                        .query(LongInterval.from(o.getInterval())).stream())
            .map(o -> ((WorkConstraintToIntervalAdapter<?>) o).workConstraint)
            .collect(Collectors.toSet());
    if (constraints.isEmpty()) {
      return test
          ? delegate.testAddShootWorks(shootWorks, ignoryShootWorks)
          : delegate.addShootWorks(shootWorks);
    }
    return new ResultAddShootWorks(null, constraints);
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    resourceConstraintMap.remove(satellite);
    return delegate.removeSatellite(satellite);
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    resourceConstraintMap.remove(groundStation);
    return delegate.removeGroundStation(groundStation);
  }

  private ResultReplaceShootWorks replaceShootWorksWithCheckWorkConstraint(
      boolean test, List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    Set<WorkConstraint<?>> brokenWorkConstraints =
        replaceShootWorkCommands.stream()
            .filter(r -> r.getTryToAddOperation() != null)
            .flatMap(r -> operationsFrom(r.getTryToAddOperation()).stream())
            .filter(
                o ->
                    (o.getResource() instanceof Satellite)
                        || (o.getResource() instanceof GroundStation))
            .flatMap(
                o ->
                    resourceConstraintMap.getOrDefault(o.getResource(), new IntervalTree<>())
                        .query(LongInterval.from(o.getInterval())).stream())
            .map(o -> ((WorkConstraintToIntervalAdapter<?>) o).workConstraint)
            .collect(Collectors.toSet());

    if (brokenWorkConstraints.isEmpty()) {
      return test
          ? delegate.testReplaceShootWorks(replaceShootWorkCommands)
          : delegate.replaceShootWorks(replaceShootWorkCommands);
    }

    return new ResultReplaceShootWorks(
        new HashSet<>(), new ResultAddShootWorks(null, brokenWorkConstraints));
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorksWithCheckWorkConstraint(false, replaceShootWorkCommands);
  }

  @Override
  public ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorksWithCheckWorkConstraint(true, replaceShootWorkCommands);
  }

  @EqualsAndHashCode(of = "workConstraint", callSuper = true)
  public static class WorkConstraintToIntervalAdapter<ResourceT> extends LongInterval {
    private WorkConstraint<ResourceT> workConstraint;

    WorkConstraintToIntervalAdapter(WorkConstraint<ResourceT> workConstraint) {
      super(
          workConstraint.getInterval().getStartMillis(),
          workConstraint.getInterval().getEndMillis());
      this.workConstraint = workConstraint;
    }
  }
}
