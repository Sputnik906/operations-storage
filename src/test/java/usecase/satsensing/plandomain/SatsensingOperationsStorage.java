package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationContainer;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResourcesOperationsStorage;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultAddOperations;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultReplaceOperations;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.OperationIntersectConfig;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.OperationIntersectContainerImpl;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume.KeepVolumeContainerImpl;
import com.gitlab.sputnik906.operationsstorage.operationtreeimpl.intervaltree.OperationTreeOnIntervalTree;
import com.lodborg.intervaltree.IntervalTree;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.joda.time.Interval;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

public class SatsensingOperationsStorage implements ISatsensingScheduleFacade {

  private final Function<Object, IOperationContainer> operationContainerFactory =
      (resource) -> {
        if (resource instanceof Satellite)
          return new SatelliteOperationContainer((Satellite) resource);
        if (resource instanceof GroundStation)
          return new GroundStationOperationContainer((GroundStation) resource);
        if (resource instanceof SatelliteMemory)
          return new SatelliteMemoryOperationContainer((SatelliteMemory) resource);
        throw new UnsupportedOperationException("Type resource " + resource + " not support");
      };

  private final ResourcesOperationsStorage operationsStorage;

  public SatsensingOperationsStorage(
      Map<Object, IOperationContainer> resourceToOperationContainerMap) {
    Objects.requireNonNull(resourceToOperationContainerMap);
    this.operationsStorage =
        new ResourcesOperationsStorage(resourceToOperationContainerMap, operationContainerFactory);
  }

  @Override
  public boolean addGroundStation(GroundStation groundStation) {
    return operationsStorage.addResource(groundStation);
  }

  @Override
  public boolean addGroundStations(Collection<GroundStation> groundStations) {
    groundStations.forEach(this::addGroundStation);
    return true;
  }

  @Override
  public Set<GroundStation> getGroundStations() {
    return operationsStorage.getAllResources().stream()
        .filter(r -> r instanceof GroundStation)
        .map(r -> (GroundStation) r)
        .collect(Collectors.toSet());
  }

  @Override
  public Set<Satellite> getSatellites() {
    return operationsStorage.getAllResources().stream()
        .filter(r -> r instanceof Satellite)
        .map(r -> (Satellite) r)
        .collect(Collectors.toSet());
  }

  @Override
  public boolean addSatellite(Satellite satellite) {
    return operationsStorage.addResource(satellite)
        && operationsStorage.addResource(new SatelliteMemory(satellite));
  }

  @Override
  public boolean addSatellites(Collection<Satellite> satellites) {
    satellites.forEach(this::addSatellite);
    return true;
  }

  @Override
  public Optional<Set<ShootWork>> addGroundStationConstraint(
      GroundStation groundStation, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<Set<ShootWork>> addSatelliteConstraint(Satellite satellite, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeGroundStationConstraint(GroundStation groundStation, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeSatelliteConstraint(Satellite satellite, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Set<WorkConstraint<GroundStation>> getGroundStationConstraints(
      GroundStation groundStation, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Set<WorkConstraint<Satellite>> getSatelliteConstraints(
      Satellite satellite, Interval interval) {
    throw new UnsupportedOperationException();
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    return addShootWorks(false, shootWorks, new HashSet<>());
  }

  private ResultAddShootWorks addShootWorks(
      boolean test, List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {

    List<IOperation<?>> operations =
        shootWorks.stream()
            .flatMap(shootWork -> operationsFrom(shootWork).stream())
            .collect(Collectors.toList());

    Set<IOperation<?>> ignoryOperations =
        ignoryShootWorks.stream()
            .flatMap(shootWork -> operationsFrom(shootWork).stream())
            .collect(Collectors.toSet());

    ResultAddOperations resultAddOperations =
        test
            ? operationsStorage.testCommitOperations(operations, ignoryOperations)
            : operationsStorage.commitOperations(operations);

    return new ResultAddShootWorks(resultAddOperations, null);
  }

  @Override
  public ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    return addShootWorks(true, shootWorks, ignoryShootWorks);
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite) {
    return operationsStorage.getOperationsByResource(satellite);
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite, Interval intersectedInterval) {
    return operationsStorage.getIntersectedOperationsByResource(satellite, intersectedInterval);
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation) {
    return operationsStorage.getOperationsByResource(groundStation);
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation, Interval intersectedInterval) {
    return operationsStorage.getIntersectedOperationsByResource(groundStation, intersectedInterval);
  }

  private Optional<Set<ShootWork>> getShootWorksByResource(Object satOrGs) {
    return getShootWorksByResource(satOrGs, null);
  }

  private <ResourceT> Optional<Set<ShootWork>> getShootWorksByResource(
      ResourceT satOrGs, Interval interval) {
    Optional<Set<OperationWithShootWork<ResourceT>>> operations =
        interval != null
            ? operationsStorage.getIntersectedOperationsByResource(satOrGs, interval)
            : operationsStorage.getOperationsByResource(satOrGs);
    return operations.map(
        operationWithShootWorks ->
            operationWithShootWorks.stream()
                .map(OperationWithShootWork::getShootWork)
                .collect(Collectors.toSet()));
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksByGroundStation(
      GroundStation groundStation, Interval interval) {
    return getShootWorksByResource(groundStation, interval);
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksBySatellite(Satellite satellite, Interval interval) {
    return getShootWorksByResource(satellite, interval);
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    SatelliteOperationContainer satelliteOperationContainer =
        (SatelliteOperationContainer) operationsStorage.removeResource(satellite);
    operationsStorage.removeResource(new SatelliteMemory(satellite));
    if (satelliteOperationContainer == null) return Optional.empty();

    return Optional.of(
        satelliteOperationContainer.getOperations().stream()
            .filter(o -> o instanceof ShootOperation)
            .map(OperationWithShootWork::getShootWork)
            .peek(
                shootWork ->
                    operationsStorage.removeOperations(
                        Collections.singletonList(new UploadOperation(shootWork))))
            .collect(Collectors.toSet()));
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    GroundStationOperationContainer groundStationOperationContainer =
        (GroundStationOperationContainer) operationsStorage.removeResource(groundStation);
    if (groundStationOperationContainer == null) return Optional.empty();

    return Optional.of(
        groundStationOperationContainer.getOperations().stream()
            .map(OperationWithShootWork::getShootWork)
            .peek(
                shootWork ->
                    operationsStorage.removeOperations(
                        Arrays.asList(
                            new ShootOperation(shootWork),
                            new DownloadOperation(shootWork),
                            new KeepImageInSatelliteOperation(shootWork))))
            .collect(Collectors.toSet()));
  }

  @Override
  public boolean removeShootWork(ShootWork shootWork) {
    Set<IOperation<?>> removedOperations =
        operationsStorage.removeOperations(operationsFrom(shootWork));
    if (removedOperations.size() > 0 && removedOperations.size() != 4) {
      throw new IllegalStateException(
          "Не консистентный план. Для shootWork должны либо быть все операции, либо отсуствовать");
    }

    return removedOperations.size() > 0;
  }

  @Override
  public Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks) {
    return shootWorks.stream().filter(this::removeShootWork).collect(Collectors.toSet());
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorks(false, replaceShootWorkCommands);
  }

  private ResultReplaceShootWorks replaceShootWorks(
      boolean test, List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    List<ReplaceOperationCommand<? extends IOperation<?>>> replaceOperationCommands =
        new ArrayList<>();
    // List replaceOperationCommands = new ArrayList<>();

    for (ReplaceOperationCommand<ShootWork> replaceShootWork : replaceShootWorkCommands) {
      if (replaceShootWork.getTryToRemoveOperation() != null) {

        replaceOperationCommands.addAll(
            operationsFrom(replaceShootWork.getTryToRemoveOperation()).stream()
                .map(ReplaceOperationCommand::remove)
                .collect(Collectors.toList()));
      }

      if (replaceShootWork.getTryToAddOperation() != null) {

        replaceOperationCommands.addAll(
            operationsFrom(replaceShootWork.getTryToAddOperation()).stream()
                .map(ReplaceOperationCommand::add)
                .collect(Collectors.toList()));
      }
    }

    ResultReplaceOperations resultReplaceOperations =
        test
            ? operationsStorage.testReplace(replaceOperationCommands)
            : operationsStorage.replace(replaceOperationCommands);

    return new ResultReplaceShootWorks(
        resultReplaceOperations.getUnremovedOperations(),
        new ResultAddShootWorks(resultReplaceOperations.getResultAddOperations(), null));
  }

  @Override
  public ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorks(true, replaceShootWorkCommands);
  }

  @Override
  public Optional<ShootWork> getShootWork(ShootPointObjectTask task) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsShootWork(ShootWork shootWork) {
    return operationsStorage.containAllOperations(operationsFrom(shootWork));
  }

  @Override
  public Set<ShootWork> getAllShootWorks() {
    return getGroundStations().stream()
        .flatMap(gs -> getShootWorksByResource(gs).orElse(new HashSet<>()).stream())
        .collect(Collectors.toSet());
  }

  public static class SatelliteOperationContainer
      extends OperationIntersectContainerImpl<Satellite, OperationWithShootWork<Satellite>> {

    public SatelliteOperationContainer(Satellite resource) {
      super(
          new OperationTreeOnIntervalTree<>(new IntervalTree<>()),
          resource,
          OperationIntersectConfig.cantIntercectedAllOperations());
    }
  }

  public static class GroundStationOperationContainer
      extends OperationIntersectContainerImpl<
          GroundStation, OperationWithShootWork<GroundStation>> {

    public GroundStationOperationContainer(GroundStation resource) {
      super(
          new OperationTreeOnIntervalTree<>(new IntervalTree<>()),
          resource,
          OperationIntersectConfig.cantIntercectedAllOperations());
    }
  }

  public static class SatelliteMemoryOperationContainer
      extends KeepVolumeContainerImpl<SatelliteMemory> {

    public SatelliteMemoryOperationContainer(SatelliteMemory satelliteMemory) {
      super(new OperationTreeOnIntervalTree<>(new IntervalTree<>()), satelliteMemory);
    }
  }
}
