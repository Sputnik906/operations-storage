package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootWork;

public abstract class AbstractSatsensingScheduleBufferEventsDelegate
    extends AbstractSatsensingScheduleDelegate {

  private final int maxSizeBuffer;

  private List<ReplaceOperationCommand<ShootWork>> bufferEvents = new ArrayList<>();

  AbstractSatsensingScheduleBufferEventsDelegate(
      ISatsensingScheduleFacade delegate, int maxSizeBuffer) {
    super(delegate);
    if (!(maxSizeBuffer > 0))
      throw new IllegalArgumentException("maxSizeEventsLog should be greater then 0");
    this.maxSizeBuffer = maxSizeBuffer;
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    ResultAddShootWorks result = delegate.addShootWorks(shootWorks);
    if (result.isSuccessful())
      addEvents(shootWorks.stream().map(ReplaceOperationCommand::add).collect(Collectors.toList()));
    return result;
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    Optional<Set<ShootWork>> result = delegate.removeSatellite(satellite);
    result.ifPresent(
        r ->
            addEvents(
                r.stream().map(ReplaceOperationCommand::remove).collect(Collectors.toList())));
    return result;
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    Optional<Set<ShootWork>> result = delegate.removeGroundStation(groundStation);
    result.ifPresent(
        r ->
            addEvents(
                r.stream().map(ReplaceOperationCommand::remove).collect(Collectors.toList())));
    return result;
  }

  @Override
  public boolean removeShootWork(ShootWork shootWork) {
    boolean result = delegate.removeShootWork(shootWork);
    if (result) addEvents(Collections.singletonList(ReplaceOperationCommand.remove(shootWork)));
    return result;
  }

  @Override
  public Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks) {
    Set<ShootWork> result = delegate.removeShootWorks(shootWorks);
    addEvents(result.stream().map(ReplaceOperationCommand::remove).collect(Collectors.toList()));
    return result;
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    ResultReplaceShootWorks result = delegate.replaceShootWorks(replaceShootWorkCommands);
    if (result.isSuccessful()) addEvents(replaceShootWorkCommands);
    return result;
  }

  private void addEvents(List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    bufferEvents.addAll(replaceShootWorkCommands);
    if (bufferEvents.size() >= maxSizeBuffer) {
      filledBufferEventsHandler(this, bufferEvents);
      bufferEvents = new ArrayList<>();
    }
  }

  protected abstract void filledBufferEventsHandler(
      ISatsensingScheduleFacade satsensingScheduleFacade,
      List<ReplaceOperationCommand<ShootWork>> bufferEvents);
}
