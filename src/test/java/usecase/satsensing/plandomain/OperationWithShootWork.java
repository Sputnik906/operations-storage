package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.BaseOperation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.joda.time.Interval;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

@Getter
@EqualsAndHashCode(callSuper = true)
public class OperationWithShootWork<ResourceT> extends BaseOperation<ResourceT> {

  private final ShootWork shootWork;

  public OperationWithShootWork(ShootWork shootWork, ResourceT resource, Interval interval) {
    super(resource, interval);
    this.shootWork = shootWork;
  }

  public ShootPointObjectTask getTask() {
    return shootWork.getShootWorkOption().getTask();
  }
}
