package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joda.time.Interval;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class SyncSatsensingScheduleDelegate implements ISatsensingScheduleFacade {
  @NonNull private final ISatsensingScheduleFacade wrapped;

  private final ReadWriteLock lock = new ReentrantReadWriteLock();

  @Override
  public boolean addGroundStation(GroundStation groundStation) {
    lock.writeLock().lock();
    try {
      return wrapped.addGroundStation(groundStation);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public boolean addGroundStations(Collection<GroundStation> groundStations) {
    lock.writeLock().lock();
    try {
      return wrapped.addGroundStations(groundStations);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Set<GroundStation> getGroundStations() {
    lock.readLock().lock();
    try {
      return wrapped.getGroundStations();
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Set<Satellite> getSatellites() {
    lock.readLock().lock();
    try {
      return wrapped.getSatellites();
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public boolean addSatellite(Satellite satellite) {
    lock.writeLock().lock();
    try {
      return wrapped.addSatellite(satellite);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public boolean addSatellites(Collection<Satellite> satellites) {
    lock.writeLock().lock();
    try {
      return wrapped.addSatellites(satellites);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Optional<Set<ShootWork>> addGroundStationConstraint(
      GroundStation groundStation, Interval interval) {
    lock.writeLock().lock();
    try {
      return wrapped.addGroundStationConstraint(groundStation, interval);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Optional<Set<ShootWork>> addSatelliteConstraint(Satellite satellite, Interval interval) {
    lock.writeLock().lock();
    try {
      return wrapped.addSatelliteConstraint(satellite, interval);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public boolean removeGroundStationConstraint(GroundStation groundStation, Interval interval) {
    lock.writeLock().lock();
    try {
      return wrapped.removeGroundStationConstraint(groundStation, interval);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public boolean removeSatelliteConstraint(Satellite satellite, Interval interval) {
    lock.writeLock().lock();
    try {
      return wrapped.removeSatelliteConstraint(satellite, interval);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Set<WorkConstraint<GroundStation>> getGroundStationConstraints(
      GroundStation groundStation, Interval interval) {
    lock.readLock().lock();
    try {
      return wrapped.getGroundStationConstraints(groundStation, interval);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Set<WorkConstraint<Satellite>> getSatelliteConstraints(
      Satellite satellite, Interval interval) {
    lock.readLock().lock();
    try {
      return wrapped.getSatelliteConstraints(satellite, interval);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    lock.writeLock().lock();
    try {
      return wrapped.addShootWorks(shootWorks);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    lock.readLock().lock();
    try {
      return wrapped.testAddShootWorks(shootWorks, ignoryShootWorks);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite) {
    lock.readLock().lock();
    try {
      return wrapped.getOperationsBySatellite(satellite);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite, Interval intersectedInterval) {
    lock.readLock().lock();
    try {
      return wrapped.getOperationsBySatellite(satellite, intersectedInterval);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation) {
    lock.readLock().lock();
    try {
      return wrapped.getOperationsByGroundStation(groundStation);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation, Interval intersectedInterval) {
    lock.readLock().lock();
    try {
      return wrapped.getOperationsByGroundStation(groundStation, intersectedInterval);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksByGroundStation(
      GroundStation groundStation, Interval interval) {
    return Optional.empty();
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksBySatellite(Satellite satellite, Interval interval) {
    return Optional.empty();
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    lock.writeLock().lock();
    try {
      return wrapped.removeSatellite(satellite);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    lock.writeLock().lock();
    try {
      return wrapped.removeGroundStation(groundStation);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public boolean removeShootWork(ShootWork shootWork) {
    lock.writeLock().lock();
    try {
      return wrapped.removeShootWork(shootWork);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks) {
    lock.writeLock().lock();
    try {
      return wrapped.removeShootWorks(shootWorks);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    lock.writeLock().lock();
    try {
      return wrapped.replaceShootWorks(replaceShootWorkCommands);
    } finally {
      lock.writeLock().unlock();
    }
  }

  @Override
  public ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    lock.readLock().lock();
    try {
      return wrapped.testReplaceShootWorks(replaceShootWorkCommands);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Optional<ShootWork> getShootWork(ShootPointObjectTask task) {
    lock.readLock().lock();
    try {
      return wrapped.getShootWork(task);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public boolean containsShootWork(ShootWork shootWork) {
    lock.readLock().lock();
    try {
      return wrapped.containsShootWork(shootWork);
    } finally {
      lock.readLock().unlock();
    }
  }

  @Override
  public Set<ShootWork> getAllShootWorks() {
    lock.readLock().lock();
    try {
      return wrapped.getAllShootWorks();
    } finally {
      lock.readLock().unlock();
    }
  }
}
