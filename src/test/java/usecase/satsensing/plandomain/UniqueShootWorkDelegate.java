package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

public class UniqueShootWorkDelegate extends AbstractSatsensingScheduleDelegate {

  private final Map<ShootPointObjectTask, ShootWork> taskShootWorkMap;

  UniqueShootWorkDelegate(
      ISatsensingScheduleFacade delegate, Map<ShootPointObjectTask, ShootWork> taskShootWorkMap) {

    super(delegate);
    Objects.requireNonNull(taskShootWorkMap);
    // Принимаем к качестве параметра только различные реализации Map taskShootWorkMap. Но сама map
    // должны быть пустой
    if (taskShootWorkMap.size() > 0) throw new IllegalArgumentException();
    this.taskShootWorkMap = taskShootWorkMap;

    delegate.getGroundStations().stream()
        .flatMap(gs -> delegate.getOperationsByGroundStation(gs).orElse(new HashSet<>()).stream())
        .map(OperationWithShootWork::getShootWork)
        .forEach(sw -> taskShootWorkMap.put(sw.getShootWorkOption().getTask(), sw));
  }

  private ResultAddShootWorks addShootWorks(
      boolean test, List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    Optional<ShootWork> alreadyExistShootWork =
        shootWorks.stream()
            .filter(sw -> !ignoryShootWorks.contains(sw))
            .filter(sw -> taskShootWorkMap.containsKey(sw.getShootWorkOption().getTask()))
            .findFirst();
    if (alreadyExistShootWork.isPresent())
      throw new IllegalArgumentException(
          "Task "
              + alreadyExistShootWork.get().getShootWorkOption().getTask()
              + " already has ShootWork");

    ResultAddShootWorks resultAddShootWorks =
        test
            ? delegate.testAddShootWorks(shootWorks, ignoryShootWorks)
            : delegate.addShootWorks(shootWorks);

    if (!test && resultAddShootWorks.isSuccessful())
      shootWorks.forEach(sw -> taskShootWorkMap.put(sw.getShootWorkOption().getTask(), sw));

    return resultAddShootWorks;
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    return addShootWorks(false, shootWorks, new HashSet<>());
  }

  @Override
  public ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    return addShootWorks(true, shootWorks, ignoryShootWorks);
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    return delegate
        .removeSatellite(satellite)
        .map(
            shootWorks ->
                shootWorks.stream()
                    .peek(sw -> taskShootWorkMap.remove(sw.getShootWorkOption().getTask()))
                    .collect(Collectors.toSet()));
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    return delegate
        .removeGroundStation(groundStation)
        .map(
            shootWorks ->
                shootWorks.stream()
                    .peek(sw -> taskShootWorkMap.remove(sw.getShootWorkOption().getTask()))
                    .collect(Collectors.toSet()));
  }

  @Override
  public Optional<ShootWork> getShootWork(ShootPointObjectTask task) {
    return Optional.ofNullable(taskShootWorkMap.get(task));
  }

  @Override
  public boolean removeShootWork(ShootWork shootWork) {
    boolean result = delegate.removeShootWork(shootWork);
    if (result) taskShootWorkMap.remove(shootWork.getShootWorkOption().getTask());
    return result;
  }

  @Override
  public Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks) {
    return shootWorks.stream().filter(this::removeShootWork).collect(Collectors.toSet());
  }

  private ResultReplaceShootWorks replaceShootWorks(
      boolean test, List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    Set<ShootPointObjectTask> checkDuplicateAddTask = new HashSet<>();

    for (ReplaceOperationCommand<ShootWork> replaceShootWork : replaceShootWorkCommands) {
      if (replaceShootWork.getTryToAddOperation() != null
          && !checkDuplicateAddTask.add(
              replaceShootWork.getTryToAddOperation().getShootWorkOption().getTask())) {
        throw new IllegalArgumentException(
            "Incorrect replaceShootWorkCommands. You try to add more than one ShootWork for task "
                + replaceShootWork.getTryToAddOperation().getShootWorkOption().getTask());
      }
    }

    ResultReplaceShootWorks resultReplaceShootWorks =
        test
            ? delegate.testReplaceShootWorks(replaceShootWorkCommands)
            : delegate.replaceShootWorks(replaceShootWorkCommands);

    if (!test && resultReplaceShootWorks.isSuccessful()) {
      replaceShootWorkCommands.forEach(
          rsw -> {
            if (rsw.getTryToRemoveOperation() != null)
              taskShootWorkMap.remove(rsw.getTryToRemoveOperation().getShootWorkOption().getTask());
          });
      replaceShootWorkCommands.forEach(
          rsw -> {
            if (rsw.getTryToAddOperation() != null)
              taskShootWorkMap.put(
                  rsw.getTryToAddOperation().getShootWorkOption().getTask(),
                  rsw.getTryToAddOperation());
          });
    }

    return resultReplaceShootWorks;
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorks(false, replaceShootWorkCommands);
  }

  @Override
  public ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return replaceShootWorks(true, replaceShootWorkCommands);
  }
}
