package usecase.satsensing.plandomain;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.gitlab.sputnik906.operationsstorage.IOperationContainer;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.lodborg.intervaltree.IntervalTree;
import de.javakaffee.kryoserializers.jodatime.JodaIntervalSerializer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.experimental.UtilityClass;
import org.joda.time.Interval;
import usecase.satsensing.domain.ShootWork;

@UtilityClass
public class SatsensingScheduleFactory {

  public static ISatsensingScheduleFacade syncWorkConstraintUniqueShootWork() {
    return sync(workConstraintUniqueShootWork(new HashMap<>(), new HashMap<>()));
  }

  public static ISatsensingScheduleFacade syncWorkConstraintUniqueShootWorkPersistent(File file) {
    ISatsensingScheduleFacade scheduleFacade =
        workConstraintUniqueShootWork(new HashMap<>(), new HashMap<>());

    Kryo kryo = new Kryo();
    kryo.register(Interval.class, new JodaIntervalSerializer());
    kryo.setRegistrationRequired(false);
    kryo.setReferences(true);

    if (file.exists()) {
      Input input = null;
      try {
        input = new Input(new FileInputStream(file));
      } catch (FileNotFoundException e) {
        throw new IllegalStateException();
      }
      Set<ShootWork> deserShootWorks = kryo.readObject(input, HashSet.class);
      input.close();
      deserShootWorks.forEach(
          sw -> {
            scheduleFacade.addSatellite(sw.getShootWorkOption().getSatellite());
            scheduleFacade.addGroundStation(sw.getShootWorkOption().getGroundStation());
          });
      ResultAddShootWorks resultAddShootWorks =
          scheduleFacade.addShootWorks(new ArrayList<>(deserShootWorks));

      if (!resultAddShootWorks.isSuccessful()) throw new IllegalStateException();
    } else {
      try {
        file.createNewFile();
      } catch (IOException e) {
        throw new IllegalStateException();
      }
    }

    return sync(
        new AbstractSatsensingScheduleBufferEventsDelegate(scheduleFacade, 10) {
          @Override
          protected void filledBufferEventsHandler(
              ISatsensingScheduleFacade satsensingScheduleFacade,
              List<ReplaceOperationCommand<ShootWork>> bufferEvents) {
            Set<ShootWork> shootWorks = satsensingScheduleFacade.getAllShootWorks();
            try {
              Output output = new Output(new FileOutputStream(file));
              kryo.writeObject(output, shootWorks);
              output.close();
            } catch (FileNotFoundException e) {
              e.printStackTrace();
            }
          }
        });
  }

  private static ISatsensingScheduleFacade sync(ISatsensingScheduleFacade delegate) {
    return new SyncSatsensingScheduleDelegate(delegate);
  }

  private static ISatsensingScheduleFacade workConstraintUniqueShootWork(
      Map<Object, IOperationContainer> resourceToOperationContainerMap,
      Map<Object, IntervalTree<Long>> resourceConstraintMap) {
    return new ResourceWorkConstraintDelegate(
        new UniqueShootWorkDelegate(
            new SatsensingOperationsStorage(resourceToOperationContainerMap), new HashMap<>()),
        resourceConstraintMap);
  }
}
