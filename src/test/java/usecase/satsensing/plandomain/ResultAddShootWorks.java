package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IResultAddOperation;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultAddOperations;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.ResultIntersectAddOperation;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume.KeepVolumeOperation;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume.ResultKeepVolumeAddOperation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import usecase.satsensing.plandomain.ISatsensingScheduleFacade.SatelliteMemory;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ResultAddShootWorks {

  private final ResultAddOperations resultAddOperations;

  private final Set<WorkConstraint<?>> brokenWorkConstraints;

  public boolean isSuccessful() {
    return (brokenWorkConstraints == null && resultAddOperations.isSuccessful());
  }

  public Set<WorkConstraint<?>> brokenWorkConstraints() {
    return brokenWorkConstraints;
  }

  public OperationWithShootWork<?> notAppliedOperation() {
    if (resultAddOperations==null) return null;//brokenWorkConstraints
    if (resultAddOperations.getNotAppliedOperation() instanceof OperationWithShootWork) {
      return (OperationWithShootWork<?>) resultAddOperations.getNotAppliedOperation();
    }
    return null;
  }

  public Set<OperationWithShootWork<?>> intersectedConflictOperations() {
    if (resultAddOperations==null) return null;//brokenWorkConstraints
    if (resultAddOperations.getReasonNotAppliedOperation() instanceof ResultIntersectAddOperation) {
      ResultIntersectAddOperation<? extends IOperation<?>> result =
          (ResultIntersectAddOperation<? extends IOperation<?>>)
              resultAddOperations.getReasonNotAppliedOperation();
      return result.getIntersectedOperations().stream()
          .map(k -> (OperationWithShootWork<?>) k)
          .collect(Collectors.toSet());
    }
    return null;
  }

  public List<List<KeepVolumeOperation<SatelliteMemory>>> memoryConflictOperations() {
    if (resultAddOperations==null) return null;//brokenWorkConstraints
    if (resultAddOperations.getReasonNotAppliedOperation()
        instanceof ResultKeepVolumeAddOperation) {

      ResultKeepVolumeAddOperation<SatelliteMemory> result =
          unsafeCast(resultAddOperations.getReasonNotAppliedOperation());

      return result.getConflictKeepVolumeOperations();
    }
    return null;
  }

  public Set<OperationWithShootWork<?>> intersectedConflictBetweenAddedOperations() {
    if (resultAddOperations==null) return null;//brokenWorkConstraints
    if (resultAddOperations.getReasonNotAppliedOperationWithAddedOperations()
        instanceof ResultIntersectAddOperation) {
      ResultIntersectAddOperation<? extends IOperation<?>> result =
          (ResultIntersectAddOperation<? extends IOperation<?>>)
              resultAddOperations.getReasonNotAppliedOperationWithAddedOperations();
      return result.getIntersectedOperations().stream()
          .map(k -> (OperationWithShootWork<?>) k)
          .collect(Collectors.toSet());
    }
    return null;
  }

  public List<List<KeepVolumeOperation<SatelliteMemory>>> memoryConflictBetweenAddedOperations() {
    if (resultAddOperations==null) return null;//brokenWorkConstraints
    if (resultAddOperations.getReasonNotAppliedOperationWithAddedOperations()
        instanceof ResultKeepVolumeAddOperation) {
      ResultKeepVolumeAddOperation<SatelliteMemory> result =
          unsafeCast(resultAddOperations.getReasonNotAppliedOperationWithAddedOperations());
      return result.getConflictKeepVolumeOperations();
    }
    return null;
  }

  private ResultKeepVolumeAddOperation<SatelliteMemory> unsafeCast(
      IResultAddOperation<? extends IOperation<?>> result) {
    return (ResultKeepVolumeAddOperation<SatelliteMemory>) result;
  }
}
