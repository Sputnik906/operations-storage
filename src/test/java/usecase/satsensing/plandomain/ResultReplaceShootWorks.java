package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import java.util.Set;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ResultReplaceShootWorks {

  @NonNull private final Set<IOperation<?>> unremovedOperations;
  @NonNull private final ResultAddShootWorks resultAddShootWorks;

  public boolean isSuccessful() {
    if (unremovedOperations.size() > 0) {
      return false;
    }
    return resultAddShootWorks.isSuccessful();
  }
}
