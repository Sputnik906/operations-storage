package usecase.satsensing.plandomain;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.WorkConstraint;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joda.time.Interval;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class AbstractSatsensingScheduleDelegate implements ISatsensingScheduleFacade {

  @NonNull protected final ISatsensingScheduleFacade delegate;

  @Override
  public boolean addGroundStation(GroundStation groundStation) {
    return delegate.addGroundStation(groundStation);
  }

  @Override
  public boolean addGroundStations(Collection<GroundStation> groundStations) {
    return delegate.addGroundStations(groundStations);
  }

  @Override
  public Set<GroundStation> getGroundStations() {
    return delegate.getGroundStations();
  }

  @Override
  public Set<Satellite> getSatellites() {
    return delegate.getSatellites();
  }

  @Override
  public boolean addSatellite(Satellite satellite) {
    return delegate.addSatellite(satellite);
  }

  @Override
  public boolean addSatellites(Collection<Satellite> satellites) {
    return delegate.addSatellites(satellites);
  }

  @Override
  public Optional<Set<ShootWork>> addGroundStationConstraint(
      GroundStation groundStation, Interval interval) {
    return delegate.addGroundStationConstraint(groundStation, interval);
  }

  @Override
  public Optional<Set<ShootWork>> addSatelliteConstraint(Satellite satellite, Interval interval) {
    return delegate.addSatelliteConstraint(satellite, interval);
  }

  @Override
  public boolean removeGroundStationConstraint(GroundStation groundStation, Interval interval) {
    return delegate.removeGroundStationConstraint(groundStation, interval);
  }

  @Override
  public boolean removeSatelliteConstraint(Satellite satellite, Interval interval) {
    return delegate.removeSatelliteConstraint(satellite, interval);
  }

  @Override
  public Set<WorkConstraint<GroundStation>> getGroundStationConstraints(
      GroundStation groundStation, Interval interval) {
    return delegate.getGroundStationConstraints(groundStation, interval);
  }

  @Override
  public Set<WorkConstraint<Satellite>> getSatelliteConstraints(
      Satellite satellite, Interval interval) {
    return delegate.getSatelliteConstraints(satellite, interval);
  }

  @Override
  public ResultAddShootWorks addShootWorks(List<ShootWork> shootWorks) {
    return delegate.addShootWorks(shootWorks);
  }

  @Override
  public ResultAddShootWorks testAddShootWorks(
      List<ShootWork> shootWorks, Set<ShootWork> ignoryShootWorks) {
    return delegate.testAddShootWorks(shootWorks, ignoryShootWorks);
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite) {
    return delegate.getOperationsBySatellite(satellite);
  }

  @Override
  public Optional<Set<OperationWithShootWork<Satellite>>> getOperationsBySatellite(
      Satellite satellite, Interval intersectedInterval) {
    return delegate.getOperationsBySatellite(satellite, intersectedInterval);
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation) {
    return delegate.getOperationsByGroundStation(groundStation);
  }

  @Override
  public Optional<Set<OperationWithShootWork<GroundStation>>> getOperationsByGroundStation(
      GroundStation groundStation, Interval intersectedInterval) {
    return delegate.getOperationsByGroundStation(groundStation, intersectedInterval);
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksByGroundStation(
      GroundStation groundStation, Interval interval) {
    return delegate.getShootWorksByGroundStation(groundStation, interval);
  }

  @Override
  public Optional<Set<ShootWork>> getShootWorksBySatellite(Satellite satellite, Interval interval) {
    return delegate.getShootWorksBySatellite(satellite, interval);
  }

  @Override
  public Optional<Set<ShootWork>> removeSatellite(Satellite satellite) {
    return delegate.removeSatellite(satellite);
  }

  @Override
  public Optional<Set<ShootWork>> removeGroundStation(GroundStation groundStation) {
    return delegate.removeGroundStation(groundStation);
  }

  @Override
  public boolean removeShootWork(ShootWork shootWork) {
    return delegate.removeShootWork(shootWork);
  }

  @Override
  public Set<ShootWork> removeShootWorks(List<ShootWork> shootWorks) {
    return delegate.removeShootWorks(shootWorks);
  }

  @Override
  public ResultReplaceShootWorks replaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return delegate.replaceShootWorks(replaceShootWorkCommands);
  }

  @Override
  public ResultReplaceShootWorks testReplaceShootWorks(
      List<ReplaceOperationCommand<ShootWork>> replaceShootWorkCommands) {
    return delegate.testReplaceShootWorks(replaceShootWorkCommands);
  }

  @Override
  public Optional<ShootWork> getShootWork(ShootPointObjectTask task) {
    return delegate.getShootWork(task);
  }

  @Override
  public boolean containsShootWork(ShootWork shootWork) {
    return delegate.containsShootWork(shootWork);
  }

  @Override
  public Set<ShootWork> getAllShootWorks() {
    return delegate.getAllShootWorks();
  }
}
