package usecase.satsensing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.joda.time.ReadableDuration;
import org.junit.jupiter.api.Test;
import usecase.satsensing.domain.GroundStation;
import usecase.satsensing.domain.Satellite;
import usecase.satsensing.domain.ShootPointObjectTask;
import usecase.satsensing.domain.ShootWork;
import usecase.satsensing.domain.ShootWorkOption;
import usecase.satsensing.plandomain.ISatsensingScheduleFacade;
import usecase.satsensing.plandomain.OperationWithShootWork;
import usecase.satsensing.plandomain.ResultReplaceShootWorks;
import usecase.satsensing.plandomain.SatsensingOperationsStorage;
import usecase.satsensing.plandomain.SatsensingScheduleFactory;

public class SatsensingScheduleTest {

  private static void tryAndAssertConflictInformation(
      Scene scene,
      ISatsensingScheduleFacade operationsStorage,
      OperationWithShootWork conflictOperation,
      int currentIndexOfConflictTask,
      List<ReplaceOperationCommand<ShootWork>> initReplaces,
      List<ConflictInformation> conflictInformations) {

    List<ReplaceOperationCommand<ShootWork>> replaces = new ArrayList<>(initReplaces);

    for (ConflictInformation conflictInformation : conflictInformations) {
      replaces.add(
          ReplaceOperationCommand.replace(
              conflictOperation.getShootWork(),
              scene
                  .getOrderedListShootWorkOptions(conflictOperation.getTask())
                  .get(++currentIndexOfConflictTask)
                  .toShootWorkHoldAllVisible()));

      if (conflictInformation.typeConflict.equals(TypeConflict.BetweenAddedOperations)) {
        assertConflictBetweenAddedOperations(
            operationsStorage.replaceShootWorks(replaces),
            conflictInformation.operationClass,
            conflictInformation.taskOfOperationWitchCantAdd,
            conflictInformation.taskOfConflictOperation);
      }

      if (conflictInformation.typeConflict.equals(TypeConflict.IntersectedAnotheOperations)) {
        assertConflictIntersectedOperations(
            operationsStorage.replaceShootWorks(replaces),
            conflictInformation.operationClass,
            conflictInformation.taskOfOperationWitchCantAdd,
            conflictInformation.taskOfConflictOperation);
      }

      replaces.remove(replaces.size() - 1);
    }
  }

  private static void assertConflictBetweenAddedOperations(
      ResultReplaceShootWorks resultReplaceShootWorks,
      Class<?> operationClass,
      ShootPointObjectTask taskOfOperationWitchCantAdd,
      ShootPointObjectTask taskOfConflictOperation) {

    assertFalse(
        resultReplaceShootWorks
            .getResultAddShootWorks()
            .intersectedConflictBetweenAddedOperations()
            .isEmpty(),
        "Должны быть конфликт между добавляемыми операциями");

    OperationWithShootWork noValidOperation =
        resultReplaceShootWorks.getResultAddShootWorks().notAppliedOperation();

    assertEquals(noValidOperation.getClass(), operationClass);

    assertEquals(taskOfOperationWitchCantAdd, noValidOperation.getTask());

    OperationWithShootWork reasonOperationOfConflictBetweenAddedOperations =
        resultReplaceShootWorks.getResultAddShootWorks().intersectedConflictBetweenAddedOperations()
            .stream()
            .findFirst()
            .get();

    assertEquals(
        taskOfConflictOperation, reasonOperationOfConflictBetweenAddedOperations.getTask());
  }

  private static void assertConflictIntersectedOperations(
      ResultReplaceShootWorks resultReplaceShootWorks,
      Class<?> operationClass,
      ShootPointObjectTask taskOfOperationWitchCantAdd,
      ShootPointObjectTask taskOfConflictOperation) {

    assertFalse(
        resultReplaceShootWorks.getResultAddShootWorks().intersectedConflictOperations().isEmpty(),
        "Должны быть конфликт между уже существующими операциями по пересечению");

    assertEquals(
        operationClass,
        resultReplaceShootWorks.getResultAddShootWorks().notAppliedOperation().getClass());

    assertEquals(
        taskOfOperationWitchCantAdd,
        resultReplaceShootWorks.getResultAddShootWorks().notAppliedOperation().getTask());
    assertEquals(
        taskOfConflictOperation,
        resultReplaceShootWorks.getResultAddShootWorks().intersectedConflictOperations().stream()
            .findFirst()
            .get()
            .getTask());
  }

  private static ISatsensingScheduleFacade initSyncSchedule(
      Scene scene, List<ShootWork> shootWorks) {
    ISatsensingScheduleFacade schedule =
        SatsensingScheduleFactory.syncWorkConstraintUniqueShootWork();
    schedule.addGroundStations(scene.groundStations);
    schedule.addSatellites(scene.satellites);
    assertTrue(schedule.addShootWorks(shootWorks).isSuccessful());
    return schedule;
  }

  private static List<ShootWork> planEachTaskOnEqualIndexSatAndGS2(Scene scene) {
    return IntStream.range(0, scene.tasks.size())
        .mapToObj(
            taskIndex ->
                scene.getOrderedListShootWorkOptions(scene.tasks.get(taskIndex)).stream()
                    .filter(
                        swo ->
                            swo.getSatellite().equals(scene.satellites.get(taskIndex))
                                && swo.getGroundStation()
                                    .equals(scene.groundStations.get(taskIndex)))
                    .findFirst()
                    .map(ShootWorkOption::toShootWorkHoldAllVisible)
                    .orElse(null))
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  private static void assertPlanEachTaskOnEqualIndexSatAndGS(
      ISatsensingScheduleFacade schedule, Scene scene) {
    IntStream.range(0, scene.tasks.size())
        .forEach(
            taskIndex ->
                assertTrue(
                    schedule
                        .getShootWork(scene.tasks.get(taskIndex))
                        .filter(
                            sw ->
                                sw.getShootWorkOption()
                                        .getSatellite()
                                        .equals(scene.satellites.get(taskIndex))
                                    && sw.getShootWorkOption()
                                        .getGroundStation()
                                        .equals(scene.groundStations.get(taskIndex)))
                        .isPresent()));
  }

  private static Scene from(int count, Instant baseTime) {
    ReadableDuration durationShootVisible = Duration.standardSeconds(10);
    ReadableDuration durationBeetweenShootVisibleEndShootAndStartDropVisible =
        Duration.standardSeconds(100);
    ReadableDuration durationDropVisible = Duration.standardSeconds(100);

    final long satelliteMemmory = 350;

    final long imageValue = 50;

    final int countShift = 2;

    ReadableDuration shiftDuration = Duration.standardSeconds(300);

    List<Satellite> satellites =
        IntStream.range(0, count)
            .mapToObj(i -> new Satellite("Satellite " + i, satelliteMemmory))
            .collect(Collectors.toList());
    List<GroundStation> groundStations =
        IntStream.range(0, count)
            .mapToObj(i -> new GroundStation("Ground station " + i))
            .collect(Collectors.toList());
    List<ShootPointObjectTask> tasks =
        IntStream.range(0, count)
            .mapToObj(i -> new ShootPointObjectTask("Task " + i))
            .collect(Collectors.toList());

    Map<ShootPointObjectTask, List<ShootWorkOption>> taskShootWorkOptionsMap = new HashMap<>();

    for (int taskIndex = 0; taskIndex < tasks.size(); taskIndex++) {
      List<ShootWorkOption> orderedShootWorks = new ArrayList<>();

      for (int shiftIndex = 0; shiftIndex < countShift; shiftIndex++) {
        Instant currentBaseTime = baseTime.plus(shiftDuration.getMillis() * shiftIndex);
        for (int i = 0; i < satellites.size(); i++) {
          // int satelliteIndex = (i+taskIndex)%satellites.size();
          int satelliteIndex = i;
          for (int j = 0; j < groundStations.size(); j++) {
            // int groundStationIndex = (i+taskIndex)%groundStations.size();
            int groundStationIndex = j;
            Interval shootVisible = new Interval(currentBaseTime, durationShootVisible);
            orderedShootWorks.add(
                new ShootWorkOption(
                    tasks.get(taskIndex),
                    satellites.get(satelliteIndex),
                    shootVisible,
                    groundStations.get(groundStationIndex),
                    new Interval(
                        Instant.ofEpochMilli(
                            shootVisible.getEndMillis()
                                + durationBeetweenShootVisibleEndShootAndStartDropVisible
                                    .getMillis()),
                        durationDropVisible),
                    imageValue));
          }
        }
      }
      taskShootWorkOptionsMap.put(tasks.get(taskIndex), orderedShootWorks);
    }

    return new Scene(satellites, groundStations, tasks, taskShootWorkOptionsMap, baseTime);
  }

  @Test
  public void replaceTest() {
    Scene scene = from(3, Instant.parse("2019-01-01T00:00:00Z"));

    // Sat0 - GS0  <- Task 0
    // Sat0 - GS1
    // Sat0 - GS2
    // Sat1 - GS0
    // Sat1 - GS1  <- Task 1
    // Sat1 - GS2
    // Sat2 - GS0
    // Sat2 - GS1
    // Sat2 - GS2  <- Task 2
    ISatsensingScheduleFacade schedule =
        initSyncSchedule(scene, planEachTaskOnEqualIndexSatAndGS2(scene));

    assertPlanEachTaskOnEqualIndexSatAndGS(schedule, scene);

    int proactiveTaskIndex = 2;
    int goalPositionInListShootWorkOptions = 0;

    ShootPointObjectTask proactiveTask = scene.tasks.get(proactiveTaskIndex);

    ShootWork goalProactiveTaskShootWork =
        scene
            .getOrderedListShootWorkOptions(proactiveTask)
            .get(goalPositionInListShootWorkOptions)
            .toShootWorkHoldAllVisible();

    assertThrows(
        IllegalArgumentException.class,
        () -> schedule.addShootWorks(Collections.singletonList(goalProactiveTaskShootWork)),
        "Если задача уже запланирована, то нельзя для нее добавить еще один ShootWork");

    List<ReplaceOperationCommand<ShootWork>> replaces =
        new ArrayList<>(
            Collections.singletonList(
                ReplaceOperationCommand.replace(
                    schedule.getShootWork(proactiveTask).get(), goalProactiveTaskShootWork)));

    ResultReplaceShootWorks resultReplaceShootWorks = schedule.replaceShootWorks(replaces);

    assertFalse(resultReplaceShootWorks.isSuccessful(), "Должны мешать другие задачи: Task 0");

    assertEquals(
        1,
        resultReplaceShootWorks.getResultAddShootWorks().intersectedConflictOperations().size(),
        "Должен быть конфликт по пересечению только с одной операцией");

    OperationWithShootWork conflictOperation =
        resultReplaceShootWorks.getResultAddShootWorks().intersectedConflictOperations().stream()
            .findFirst()
            .get();

    assertEquals(scene.tasks.get(0), conflictOperation.getTask());

    int currentIndexOfConflictTask =
        scene.indexInOrderedListShootWorkOptions(
            conflictOperation.getShootWork().getShootWorkOption());

    assertEquals(0, currentIndexOfConflictTask);

    List<ConflictInformation> conflictInformations =
        Arrays.asList(
            new ConflictInformation(
                TypeConflict.BetweenAddedOperations,
                SatsensingOperationsStorage.ShootOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(proactiveTaskIndex)),
            new ConflictInformation(
                TypeConflict.BetweenAddedOperations,
                SatsensingOperationsStorage.ShootOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(proactiveTaskIndex)),
            new ConflictInformation(
                TypeConflict.BetweenAddedOperations,
                SatsensingOperationsStorage.UploadOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(proactiveTaskIndex)),
            new ConflictInformation(
                TypeConflict.IntersectedAnotheOperations,
                SatsensingOperationsStorage.ShootOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(1)),
            new ConflictInformation(
                TypeConflict.IntersectedAnotheOperations,
                SatsensingOperationsStorage.ShootOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(1)),
            new ConflictInformation(
                TypeConflict.BetweenAddedOperations,
                SatsensingOperationsStorage.UploadOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(proactiveTaskIndex)),
            new ConflictInformation(
                TypeConflict.IntersectedAnotheOperations,
                SatsensingOperationsStorage.UploadOperation.class,
                scene.tasks.get(0),
                scene.tasks.get(1)));

    tryAndAssertConflictInformation(
        scene,
        schedule,
        conflictOperation,
        currentIndexOfConflictTask,
        replaces,
        conflictInformations);

    replaces.add(
        ReplaceOperationCommand.replace(
            conflictOperation.getShootWork(),
            scene
                .getOrderedListShootWorkOptions(conflictOperation.getTask())
                .get(conflictInformations.size() + 1)
                .toShootWorkHoldAllVisible()));

    assertTrue(schedule.replaceShootWorks(replaces).isSuccessful());

    assertEquals(goalProactiveTaskShootWork, schedule.getShootWork(proactiveTask).get());
  }

  private enum TypeConflict {
    BetweenAddedOperations,
    IntersectedAnotheOperations
  }

  @RequiredArgsConstructor
  private static class Scene {
    @NonNull private final List<Satellite> satellites;
    @NonNull private final List<GroundStation> groundStations;
    @NonNull private final List<ShootPointObjectTask> tasks;
    @NonNull private final Map<ShootPointObjectTask, List<ShootWorkOption>> taskShootWorkOptionsMap;
    @NonNull private final Instant baseTime;

    public List<ShootWorkOption> getOrderedListShootWorkOptions(ShootPointObjectTask task) {
      return taskShootWorkOptionsMap.get(task);
    }

    public int indexInOrderedListShootWorkOptions(ShootWorkOption shootWorkOption) {
      return taskShootWorkOptionsMap.get(shootWorkOption.getTask()).indexOf(shootWorkOption);
    }
  }

  @RequiredArgsConstructor
  private static class ConflictInformation {
    @NonNull private final TypeConflict typeConflict;
    @NonNull private final Class<?> operationClass;
    @NonNull private final ShootPointObjectTask taskOfOperationWitchCantAdd;
    @NonNull private final ShootPointObjectTask taskOfConflictOperation;
  }
}
