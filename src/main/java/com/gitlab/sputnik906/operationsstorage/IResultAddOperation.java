package com.gitlab.sputnik906.operationsstorage;

public interface IResultAddOperation<OperationTypeT> {
  OperationTypeT getOperation();

  boolean isSuccessful();
}
