package com.gitlab.sputnik906.operationsstorage;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.joda.time.Interval;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@ToString
public class WorkConstraint<ResourceT> {
  @NonNull private final ResourceT resource;
  @NonNull private final Interval interval;
}
