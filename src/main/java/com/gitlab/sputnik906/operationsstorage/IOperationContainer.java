package com.gitlab.sputnik906.operationsstorage;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface IOperationContainer<
    ResourceT,
    OperationTypeT extends IOperation<ResourceT>,
    ResultAddOperationT extends IResultAddOperation<OperationTypeT>> {

  ResultAddOperationT testAddOperation(
      OperationTypeT operation, Set<OperationTypeT> ignoryOperations);

  ResultAddOperationT addOperation(OperationTypeT operation);

  boolean removeOperation(OperationTypeT operation);

  boolean containOperation(OperationTypeT operation);

  Set<OperationTypeT> getOperations();

  Set<OperationTypeT> intersectedOperations(org.joda.time.Interval interval);

  default boolean rollback(ResultAddOperationT resultAddOperationT) {
    return resultAddOperationT.isSuccessful()
        && removeOperation(resultAddOperationT.getOperation());
  }

  default Set<OperationTypeT> removeOperations(List<OperationTypeT> operations) {
    return operations.stream().filter(this::removeOperation).collect(Collectors.toSet());
  }
}
