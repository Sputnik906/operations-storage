package com.gitlab.sputnik906.operationsstorage;

public interface IVolumeResource {
  long minValue();

  long maxValue();
}
