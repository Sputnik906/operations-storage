package com.gitlab.sputnik906.operationsstorage;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.joda.time.Interval;

@Getter
@EqualsAndHashCode(callSuper = true)
public class VolumeOperation<ResourceT extends IVolumeResource> extends BaseOperation<ResourceT> {

  private final long volume;

  public VolumeOperation(ResourceT resourceT, Interval interval, long volume) {
    super(resourceT, interval);
    this.volume = volume;
  }
}
