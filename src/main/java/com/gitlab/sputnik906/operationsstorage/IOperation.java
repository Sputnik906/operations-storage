package com.gitlab.sputnik906.operationsstorage;

import org.joda.time.Interval;

public interface IOperation<ResourceT> {
  ResourceT getResource();

  Interval getInterval();
}
