package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationContainer;
import com.gitlab.sputnik906.operationsstorage.IOperationTree;
import com.gitlab.sputnik906.operationsstorage.IResultAddOperation;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode
public abstract class AbstractTreeOperationContainer<
        ResourceT,
        OperationTypeT extends IOperation<ResourceT>,
        ResultAddOperationT extends IResultAddOperation<OperationTypeT>>
    implements IOperationContainer<ResourceT, OperationTypeT, ResultAddOperationT> {

  @NonNull private final IOperationTree<ResourceT, OperationTypeT> operationTree;

  @NonNull private final ResourceT resourceT;

  @Override
  public ResultAddOperationT testAddOperation(
      OperationTypeT operation, Set<OperationTypeT> ignoryOperations) {
    if (!operation.getResource().equals(getResourceT())) throw new IllegalArgumentException();
    return addOperation(operation, true, ignoryOperations);
  }

  @Override
  public ResultAddOperationT addOperation(OperationTypeT operation) {
    if (!operation.getResource().equals(getResourceT())) throw new IllegalArgumentException();
    return addOperation(operation, false, new HashSet<>());
  }

  protected abstract ResultAddOperationT addOperation(
      OperationTypeT operation, boolean test, Set<OperationTypeT> ignoryOperations);

  @Override
  public boolean removeOperation(OperationTypeT operation) {
    return operationTree.remove(operation);
  }

  @Override
  public boolean containOperation(OperationTypeT operation) {
    return operationTree.contains(operation);
  }

  @Override
  public Set<OperationTypeT> getOperations() {
    return operationTree.allOperations();
  }

  @Override
  public Set<OperationTypeT> intersectedOperations(org.joda.time.Interval interval) {
    return operationTree.query(interval);
  }

  protected boolean addOperationToIntervalTree(OperationTypeT operation) {
    return operationTree.add(operation);
  }
}
