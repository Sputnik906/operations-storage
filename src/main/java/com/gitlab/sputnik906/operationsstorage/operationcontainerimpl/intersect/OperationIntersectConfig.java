package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class OperationIntersectConfig<ResourceT> {

  private final PairClassOfOperationsCantIntersected<ResourceT>[]
      pairClassOfOperationsCantIntersected;

  @SafeVarargs
  public static <ResourceT>
      OperationIntersectConfig<ResourceT> fromPairClassOfOperationsCantIntersecteds(
          PairClassOfOperationsCantIntersected<ResourceT>...
              pairClassOfOperationsCantIntersecteds) {
    return new OperationIntersectConfig<>(pairClassOfOperationsCantIntersecteds);
  }

  public static <ResourceT> OperationIntersectConfig<ResourceT> cantIntercectedAllOperations() {
    return new OperationIntersectConfig<>(null);
  }

  public static <ResourceT> OperationIntersectConfig<ResourceT> canIntercectedAllOperations() {
    return fromPairClassOfOperationsCantIntersecteds();
  }

  public boolean canIntercected(
      IOperation<ResourceT> operation1, IOperation<ResourceT> operation2) {
    if (pairClassOfOperationsCantIntersected == null) {
      return false;
    }
    return Stream.of(pairClassOfOperationsCantIntersected)
        .anyMatch(p -> p.contain(operation1.getClass(), operation2.getClass()));
  }
}
