package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class PairClassOfOperationsCantIntersected<ResourceT> {

  @NonNull private final Class<? extends IOperation<ResourceT>> classOperation1;
  @NonNull private final Class<? extends IOperation<ResourceT>> classOperation2;

  public boolean contain(Class<? extends IOperation<?>> classOperation) {
    return classOperation1.equals(classOperation) || classOperation2.equals(classOperation);
  }

  public boolean contain(Class<?> classOperation1, Class<?> classOperation2) {
    return (this.classOperation1.equals(classOperation1)
            && this.classOperation2.equals(classOperation2))
        || (this.classOperation1.equals(classOperation2)
            && this.classOperation2.equals(classOperation1));
  }
}
