package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IVolumeResource;
import com.gitlab.sputnik906.operationsstorage.VolumeOperation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.joda.time.Interval;

@Getter
@EqualsAndHashCode
public class KeepVolumeOperation<ResourceT extends IVolumeResource>
    implements IOperation<ResourceT> {

  private final VolumeOperation<ResourceT> increasingVolumeOperation;
  private final VolumeOperation<ResourceT> reducingVolumeOperation;

  private final Interval interval;

  public KeepVolumeOperation(
      VolumeOperation<ResourceT> increasingVolumeOperation,
      VolumeOperation<ResourceT> reducingVolumeOperation) {
    this.increasingVolumeOperation = increasingVolumeOperation;
    this.reducingVolumeOperation = reducingVolumeOperation;

    if (increasingVolumeOperation.getVolume() <= 0)
      throw new IllegalArgumentException("IncreasingVolumeOperation should be greater than 0");

    if (!increasingVolumeOperation.getResource().equals(reducingVolumeOperation.getResource()))
      throw new IllegalArgumentException(
          "For KeepVolumeOperation resource of both operation should be same");

    if ((increasingVolumeOperation.getVolume() + reducingVolumeOperation.getVolume()) != 0)
      throw new IllegalArgumentException("Sum of volume operations should be equal 0");

    this.interval =
        new Interval(
            increasingVolumeOperation.getInterval().getStartMillis(),
            reducingVolumeOperation.getInterval().getEndMillis());
  }

  @Override
  public ResourceT getResource() {
    return increasingVolumeOperation.getResource();
  }
}
