package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationTree;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.AbstractTreeOperationContainer;
import java.util.Set;
import java.util.stream.Collectors;

public class OperationIntersectContainerImpl<
        ResourceT, OperationTypeT extends IOperation<ResourceT>>
    extends AbstractTreeOperationContainer<
        ResourceT, OperationTypeT, ResultIntersectAddOperation<OperationTypeT>> {

  private final OperationIntersectConfig<ResourceT> config;

  public OperationIntersectContainerImpl(
      IOperationTree<ResourceT, OperationTypeT> operationTree,
      ResourceT resourceT,
      OperationIntersectConfig<ResourceT> config) {
    super(operationTree, resourceT);
    this.config = config;
  }

  protected ResultIntersectAddOperation<OperationTypeT> addOperation(
      OperationTypeT operation, boolean test, Set<OperationTypeT> ignoryOperations) {

    Set<OperationTypeT> intersectedOperations = intersectedOperations(operation.getInterval());

    Set<OperationTypeT> filteredIntersectedOperations =
        intersectedOperations.stream()
            .filter(o -> !config.canIntercected(o, operation))
            .collect(Collectors.toSet());

    if (test) {
      filteredIntersectedOperations =
          filteredIntersectedOperations.stream()
              .filter(o -> !ignoryOperations.contains(o))
              .collect(Collectors.toSet());
      return filteredIntersectedOperations.size() == 0
          ? ResultIntersectAddOperation.successful(operation)
          : ResultIntersectAddOperation.intersected(operation, filteredIntersectedOperations);
    }

    if (filteredIntersectedOperations.size() == 0) {
      addOperationToIntervalTree(operation);
      return ResultIntersectAddOperation.successful(operation);
    }

    return ResultIntersectAddOperation.intersected(operation, filteredIntersectedOperations);
  }
}
