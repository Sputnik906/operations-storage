package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.AbstractResultAddOperation;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;

@Getter
public class ResultIntersectAddOperation<OperationTypeT extends IOperation<?>>
    extends AbstractResultAddOperation<OperationTypeT> {

  private final Set<OperationTypeT> intersectedOperations;

  private ResultIntersectAddOperation(
      OperationTypeT operation, Set<OperationTypeT> intersectedOperations) {
    super(operation);
    this.intersectedOperations = intersectedOperations;
  }

  public static <OperationTypeT extends IOperation<?>>
      ResultIntersectAddOperation<OperationTypeT> successful(OperationTypeT operation) {
    return new ResultIntersectAddOperation<>(operation, new HashSet<>());
  }

  public static <OperationTypeT extends IOperation<?>>
      ResultIntersectAddOperation<OperationTypeT> intersected(
          OperationTypeT operation, Set<OperationTypeT> intersectedOperations) {
    return new ResultIntersectAddOperation<>(operation, intersectedOperations);
  }

  @Override
  public boolean isSuccessful() {
    return intersectedOperations.size() == 0;
  }
}
