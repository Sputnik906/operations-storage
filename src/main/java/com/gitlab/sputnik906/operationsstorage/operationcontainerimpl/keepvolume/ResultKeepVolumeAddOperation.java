package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume;

import com.gitlab.sputnik906.operationsstorage.IVolumeResource;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.AbstractResultAddOperation;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class ResultKeepVolumeAddOperation<ResourceT extends IVolumeResource>
    extends AbstractResultAddOperation<KeepVolumeOperation<ResourceT>> {

  private final List<List<KeepVolumeOperation<ResourceT>>> conflictKeepVolumeOperations;

  public ResultKeepVolumeAddOperation(
      KeepVolumeOperation<ResourceT> operation,
      List<List<KeepVolumeOperation<ResourceT>>> conflictKeepVolumeOperations) {
    super(operation);
    this.conflictKeepVolumeOperations = conflictKeepVolumeOperations;
  }

  public static <ResourceT extends IVolumeResource>
      ResultKeepVolumeAddOperation<ResourceT> successful(KeepVolumeOperation<ResourceT> operation) {
    return new ResultKeepVolumeAddOperation<>(operation, new ArrayList<>());
  }

  public static <ResourceT extends IVolumeResource>
      ResultKeepVolumeAddOperation<ResourceT> conflictKeepVolume(
          KeepVolumeOperation<ResourceT> operation,
          List<List<KeepVolumeOperation<ResourceT>>> conflictKeepVolumeOperations) {
    return new ResultKeepVolumeAddOperation<>(operation, conflictKeepVolumeOperations);
  }

  @Override
  public boolean isSuccessful() {
    return conflictKeepVolumeOperations.size() == 0;
  }
}
