package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.keepvolume;

import com.gitlab.sputnik906.operationsstorage.IOperationTree;
import com.gitlab.sputnik906.operationsstorage.IVolumeResource;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.AbstractTreeOperationContainer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class KeepVolumeContainerImpl<ResourceT extends IVolumeResource>
    extends AbstractTreeOperationContainer<
        ResourceT, KeepVolumeOperation<ResourceT>, ResultKeepVolumeAddOperation<ResourceT>> {

  public KeepVolumeContainerImpl(
      IOperationTree<ResourceT, KeepVolumeOperation<ResourceT>> operationTree,
      ResourceT resourceT) {
    super(operationTree, resourceT);
  }

  private static <ResourceT extends IVolumeResource>
      List<List<KeepVolumeOperation<ResourceT>>>
          findAllListOfConflictVolumeOperationsExceedTreshold(
              Set<KeepVolumeOperation<ResourceT>> intersectedVolumeOperations,
              long thresholdVolume) {

    List<KeepVolumeOperation<ResourceT>> intersectedVolumeOperationsOrderedList =
        new ArrayList<>(intersectedVolumeOperations);
    intersectedVolumeOperationsOrderedList.sort(
        Comparator.comparingLong(o -> o.getInterval().getStartMillis()));

    List<List<KeepVolumeOperation<ResourceT>>> result = new ArrayList<>();

    List<KeepVolumeOperation<ResourceT>> currentConflictVolumeIntervals = new ArrayList<>();

    result.add(currentConflictVolumeIntervals);

    long currentVolume = 0;

    for (KeepVolumeOperation<ResourceT> keepVolumeOperation :
        intersectedVolumeOperationsOrderedList) {

      List<KeepVolumeOperation<ResourceT>> alreadyEnded =
          currentConflictVolumeIntervals.stream()
              .filter(
                  v ->
                      v.getInterval().getEndMillis()
                          < keepVolumeOperation.getInterval().getStartMillis())
              .collect(Collectors.toList());

      currentConflictVolumeIntervals.removeAll(alreadyEnded);

      currentVolume -=
          alreadyEnded.stream().mapToLong(v -> v.getIncreasingVolumeOperation().getVolume()).sum();

      currentVolume += keepVolumeOperation.getIncreasingVolumeOperation().getVolume();
      currentConflictVolumeIntervals.add(keepVolumeOperation);

      if (currentVolume > thresholdVolume) {
        currentConflictVolumeIntervals = new ArrayList<>();
        result.add(currentConflictVolumeIntervals);
      }
    }

    return result;
  }

  protected ResultKeepVolumeAddOperation<ResourceT> addOperation(
      KeepVolumeOperation<ResourceT> keepVolumeOperation,
      boolean test,
      Set<KeepVolumeOperation<ResourceT>> ignoryKeepVolumeOperations) {

    Set<KeepVolumeOperation<ResourceT>> intersectedVolumeOperations =
        intersectedOperations(keepVolumeOperation.getInterval());

    if (test) {
      intersectedVolumeOperations =
          intersectedVolumeOperations.stream()
              .filter(o -> !ignoryKeepVolumeOperations.contains(o))
              .collect(Collectors.toSet());
    }

    long estimateMaxPossibleVolume =
        keepVolumeOperation.getIncreasingVolumeOperation().getVolume()
            + intersectedVolumeOperations.stream()
                .mapToLong(i -> i.getIncreasingVolumeOperation().getVolume())
                .sum();

    if (estimateMaxPossibleVolume <= getResourceT().maxValue()) {
      if (!test) addOperationToIntervalTree(keepVolumeOperation);
      return ResultKeepVolumeAddOperation.successful(keepVolumeOperation);
    }

    if (keepVolumeOperation.getIncreasingVolumeOperation().getVolume() > getResourceT().maxValue())
      throw new IllegalArgumentException(
          "Not enough volume on resource "); // TODO подумать что делать в этом случае

    List<List<KeepVolumeOperation<ResourceT>>> conflictVolumeOperations =
        findAllListOfConflictVolumeOperationsExceedTreshold(
            intersectedVolumeOperations,
            getResourceT().maxValue()
                - keepVolumeOperation.getIncreasingVolumeOperation().getVolume());

    if (conflictVolumeOperations.size() > 0) {
      return ResultKeepVolumeAddOperation.conflictKeepVolume(
          keepVolumeOperation, conflictVolumeOperations);
    }

    if (!test) addOperationToIntervalTree(keepVolumeOperation);
    return ResultKeepVolumeAddOperation.successful(keepVolumeOperation);
  }
}
