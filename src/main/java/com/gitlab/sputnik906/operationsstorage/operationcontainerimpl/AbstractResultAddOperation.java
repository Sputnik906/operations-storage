package com.gitlab.sputnik906.operationsstorage.operationcontainerimpl;

import com.gitlab.sputnik906.operationsstorage.IResultAddOperation;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public abstract class AbstractResultAddOperation<OperationTypeT>
    implements IResultAddOperation<OperationTypeT> {
  @NonNull private final OperationTypeT operation;

  @Override
  public OperationTypeT getOperation() {
    return operation;
  }
}
