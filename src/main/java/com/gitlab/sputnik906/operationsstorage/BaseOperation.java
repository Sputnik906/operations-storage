package com.gitlab.sputnik906.operationsstorage;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joda.time.Interval;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class BaseOperation<ResourceT> implements IOperation<ResourceT> {
  @NonNull private final ResourceT resource;
  @NonNull private final Interval interval;
}
