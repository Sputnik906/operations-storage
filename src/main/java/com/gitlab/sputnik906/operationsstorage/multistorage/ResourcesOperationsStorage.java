package com.gitlab.sputnik906.operationsstorage.multistorage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationContainer;
import com.gitlab.sputnik906.operationsstorage.IResultAddOperation;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ResourcesOperationsStorage {

  @NonNull private final Map<Object, IOperationContainer> resourceToOperationContainerMap;

  @NonNull private final Function<Object, IOperationContainer> operationContainerFactory;

  public boolean addResource(Object resource) {
    return resourceToOperationContainerMap.putIfAbsent(
            resource, operationContainerFactory.apply(resource))
        == null;
  }

  private <ResourceT, OperationTypeT extends IOperation<ResourceT>>
      Optional<
              IOperationContainer<?, OperationTypeT, ? extends IResultAddOperation<OperationTypeT>>>
          get(ResourceT resource) {
    return Optional.ofNullable(resourceToOperationContainerMap.get(resource));
  }

  public Set<Object> getAllResources() {
    return resourceToOperationContainerMap.keySet();
  }

  public <ResourceT, OperationTypeT extends IOperation<ResourceT>>
      Optional<Set<OperationTypeT>> getOperationsByResource(ResourceT resource) {
    IOperationContainer<?, OperationTypeT, ? extends IResultAddOperation<OperationTypeT>>
        operationContainer = resourceToOperationContainerMap.get(resource);
    if (operationContainer == null) {
      return Optional.empty();
    }
    return Optional.of(operationContainer.getOperations());
  }

  public <ResourceT, OperationTypeT extends IOperation<ResourceT>>
      Optional<Set<OperationTypeT>> getIntersectedOperationsByResource(
          ResourceT resource, org.joda.time.Interval interval) {
    IOperationContainer<ResourceT, OperationTypeT, ? extends IResultAddOperation<OperationTypeT>>
        operationContainer = resourceToOperationContainerMap.get(resource);
    if (operationContainer == null) {
      return Optional.empty();
    }
    return Optional.of(operationContainer.intersectedOperations(interval));
  }

  public <ResourceT>
      IOperationContainer<
              ResourceT,
              ? extends IOperation<ResourceT>,
              ? extends IResultAddOperation<? extends IOperation<ResourceT>>>
          removeResource(ResourceT resource) {
    return resourceToOperationContainerMap.remove(resource);
  }

  public boolean containOperation(IOperation<?> operation) {
    IOperationContainer<?, IOperation<?>, ? extends IResultAddOperation<IOperation<?>>>
        operationContainer = resourceToOperationContainerMap.get(operation.getResource());
    return operationContainer != null && operationContainer.containOperation(operation);
  }

  public boolean containAllOperations(List<IOperation<?>> operations) {
    return operations.stream().allMatch(this::containOperation);
  }

  public boolean removeOperation(IOperation<?> operation) {
    IOperationContainer<?, IOperation<?>, ? extends IResultAddOperation<IOperation<?>>>
        operationContainer = resourceToOperationContainerMap.get(operation.getResource());
    return operationContainer != null && operationContainer.removeOperation(operation);
  }

  public Set<IOperation<?>> removeOperations(List<IOperation<?>> operations) {
    return operations.stream().filter(this::removeOperation).collect(Collectors.toSet());
  }

  public ResultAddOperations testCommitOperations(
      List<IOperation<?>> operations, Set<IOperation<?>> ignoryOperations) {
    return commitOperations(true, true, operations, ignoryOperations);
  }

  public ResultAddOperations commitOperations(List<IOperation<?>> operations) {
    return commitOperations(false, true, operations, new HashSet<>());
  }

  private ResultAddOperations commitOperations(
      boolean test,
      boolean checkConflictsBetweenAddedOperations,
      List<IOperation<?>> operations,
      Set<IOperation<?>> ignoryOperations) {
    if (checkConflictsBetweenAddedOperations) {
      ResultAddOperations resultAddOperations = testOperationsNotConflictEachOther(operations);
      if (!resultAddOperations.isSuccessful()) {
        return ResultAddOperations.conflictBetweenAddedOperations(
            resultAddOperations.getNotAppliedOperation(),
            resultAddOperations.getReasonNotAppliedOperation());
      }
    }

    List<IResultAddOperation<IOperation<?>>> succesfullAddOperations = new ArrayList<>();

    for (IOperation<?> operation : operations) {
      IOperationContainer<?, IOperation<?>, ? extends IResultAddOperation<IOperation<?>>>
          operationContainer = resourceToOperationContainerMap.get(operation.getResource());

      if (operationContainer == null) {
        if (!test) {
          succesfullAddOperations.forEach(o -> removeOperation(o.getOperation()));
        }
        return ResultAddOperations.resourceAbsent(operation, operation.getResource());
      }

      IResultAddOperation<IOperation<?>> resultAddOperation =
          test
              ? operationContainer.testAddOperation(operation, ignoryOperations)
              : operationContainer.addOperation(operation);

      if (!resultAddOperation.isSuccessful()) {
        if (!test) {
          succesfullAddOperations.forEach(o -> removeOperation(o.getOperation()));
        }
        return ResultAddOperations.cantAdd(operation, resultAddOperation);
      }

      succesfullAddOperations.add(resultAddOperation);
    }

    return ResultAddOperations.successful();
  }

  public ResultAddOperations testOperationsNotConflictEachOther(List<IOperation<?>> operations) {
    ResourcesOperationsStorage operationsStorage =
        new ResourcesOperationsStorage(new HashMap<>(), operationContainerFactory);
    operations.forEach(o -> operationsStorage.addResource(o.getResource()));
    return operationsStorage.commitOperations(false, false, operations, new HashSet<>());
  }

  /**
   * Change operations
   *
   * @param replaceOperationCommands - ordered command list to apply
   * @return command result
   */
  public ResultReplaceOperations replace(
      List<ReplaceOperationCommand<? extends IOperation<?>>> replaceOperationCommands) {
    List<IOperation<?>> operationsForRemove =
        replaceOperationCommands.stream()
            .filter(r -> r.getTryToRemoveOperation() != null)
            .map(ReplaceOperationCommand::getTryToRemoveOperation)
            .collect(Collectors.toList());

    Set<IOperation<?>> removedOperations = removeOperations(operationsForRemove);

    Set<IOperation<?>> unremovedOperations =
        operationsForRemove.stream()
            .filter(o -> !removedOperations.contains(o))
            .collect(Collectors.toSet());

    if (unremovedOperations.size() > 0) {
      ResultAddOperations resultAddOperations =
          commitOperations(new ArrayList<>(removedOperations)); // откатываемся
      if (!resultAddOperations.isSuccessful()) {
        throw new IllegalStateException("Не возможно восстановить удаленные операции");
      }
      return new ResultReplaceOperations(unremovedOperations, null);
    }

    List<IOperation<?>> operationsForAdd =
        replaceOperationCommands.stream()
            .filter(r -> r.getTryToAddOperation() != null)
            .map(ReplaceOperationCommand::getTryToAddOperation)
            .collect(Collectors.toList());

    ResultAddOperations resultAddOperations =
        commitOperations(operationsForAdd); // если не удалось добавить то автоматом идет откат

    if (!resultAddOperations.isSuccessful()) {
      ResultAddOperations resultAddOperations2 =
          commitOperations(new ArrayList<>(removedOperations)); // откатываемся
      if (!resultAddOperations2.isSuccessful()) {
        throw new IllegalStateException("Не возможно восстановить удаленные операции");
      }
    }

    return new ResultReplaceOperations(unremovedOperations, resultAddOperations);
  }

  public ResultReplaceOperations testReplace(
      List<ReplaceOperationCommand<? extends IOperation<?>>> replaceOperationCommands) {
    List<IOperation<?>> operationsForRemove =
        replaceOperationCommands.stream()
            .filter(r -> r.getTryToRemoveOperation() != null)
            .map(ReplaceOperationCommand::getTryToRemoveOperation)
            .collect(Collectors.toList());

    Set<IOperation<?>> notContainsOperations =
        operationsForRemove.stream().filter(o -> !containOperation(o)).collect(Collectors.toSet());

    if (notContainsOperations.size() > 0) {
      return new ResultReplaceOperations(notContainsOperations, null);
    }

    List<IOperation<?>> operationsForAdd =
        replaceOperationCommands.stream()
            .filter(r -> r.getTryToAddOperation() != null)
            .map(ReplaceOperationCommand::getTryToAddOperation)
            .collect(Collectors.toList());

    ResultAddOperations resultAddOperations =
        testCommitOperations(operationsForAdd, new HashSet<>(operationsForRemove));

    return new ResultReplaceOperations(notContainsOperations, resultAddOperations);
  }
}
