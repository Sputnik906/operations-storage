package com.gitlab.sputnik906.operationsstorage.multistorage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IResultAddOperation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ResultAddOperations {

  private final IOperation<?> notAppliedOperation;

  private final Object absentResource;

  private final IResultAddOperation<? extends IOperation<?>> reasonNotAppliedOperation;

  private final IResultAddOperation<? extends IOperation<?>>
      reasonNotAppliedOperationWithAddedOperations;

  public static ResultAddOperations successful() {
    return new ResultAddOperations(null, null, null, null);
  }

  public static ResultAddOperations resourceAbsent(IOperation<?> operation, Object absentResource) {
    return new ResultAddOperations(operation, absentResource, null, null);
  }

  public static ResultAddOperations cantAdd(
      IOperation<?> operation,
      IResultAddOperation<? extends IOperation<?>> reasonNotAppliedOperation) {
    return new ResultAddOperations(operation, null, reasonNotAppliedOperation, null);
  }

  public static ResultAddOperations conflictBetweenAddedOperations(
      IOperation<?> operation,
      IResultAddOperation<? extends IOperation<?>> reasonNotAppliedOperationWithAddedOperations) {
    return new ResultAddOperations(
        operation, null, null, reasonNotAppliedOperationWithAddedOperations);
  }

  public boolean isSuccessful() {
    return absentResource == null
        && reasonNotAppliedOperation == null
        && reasonNotAppliedOperationWithAddedOperations == null;
  }
}
