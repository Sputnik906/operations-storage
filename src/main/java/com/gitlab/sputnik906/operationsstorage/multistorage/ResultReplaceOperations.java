package com.gitlab.sputnik906.operationsstorage.multistorage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import java.util.Set;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ResultReplaceOperations {

  @NonNull private final Set<IOperation<?>> unremovedOperations;

  private final ResultAddOperations resultAddOperations;

  public boolean isSuccessful() {
    if (unremovedOperations.size() > 0) {
      return false;
    }
    return resultAddOperations.isSuccessful();
  }
}
