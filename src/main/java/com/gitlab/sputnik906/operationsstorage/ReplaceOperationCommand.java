package com.gitlab.sputnik906.operationsstorage;

import java.util.Objects;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Getter
public class ReplaceOperationCommand<OperationTypeT> {
  private final OperationTypeT tryToRemoveOperation;

  private final OperationTypeT tryToAddOperation;

  public static <OperationTypeT> ReplaceOperationCommand<OperationTypeT> add(
      OperationTypeT tryToAddOperation) {
    Objects.requireNonNull(tryToAddOperation);
    return new ReplaceOperationCommand<>(null, tryToAddOperation);
  }

  public static <OperationTypeT> ReplaceOperationCommand<OperationTypeT> remove(
      OperationTypeT tryToRemoveOperation) {
    Objects.requireNonNull(tryToRemoveOperation);
    return new ReplaceOperationCommand<>(tryToRemoveOperation, null);
  }

  public static <OperationTypeT> ReplaceOperationCommand<OperationTypeT> replace(
      OperationTypeT tryToRemoveOperation, OperationTypeT tryToAddOperation) {
    Objects.requireNonNull(tryToRemoveOperation);
    Objects.requireNonNull(tryToAddOperation);
    return new ReplaceOperationCommand<>(tryToRemoveOperation, tryToAddOperation);
  }

  public ReplaceOperationCommand<OperationTypeT> revert() {
    return new ReplaceOperationCommand<>(tryToAddOperation, tryToRemoveOperation);
  }
}
