package com.gitlab.sputnik906.operationsstorage.operationtreeimpl.intervaltree;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationTree;
import com.lodborg.intervaltree.Interval;
import com.lodborg.intervaltree.IntervalTree;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OperationTreeOnIntervalTree<ResourceT, OperationTypeT extends IOperation<ResourceT>>
    implements IOperationTree<ResourceT, OperationTypeT> {

  @NonNull private final IntervalTree<Long> intervalTree;

  @Override
  public boolean add(OperationTypeT operation) {
    return intervalTree.add(new OperationToIntervalAdapter<>(operation));
  }

  @Override
  public boolean remove(OperationTypeT operation) {
    // Странно, что если мы что-то удалили то возращается false, если размер не изменился то
    // возвращается true
    return !intervalTree.remove(new OperationToIntervalAdapter<>(operation));
  }

  @Override
  public boolean contains(OperationTypeT operation) {
    LongInterval interval =
        new LongInterval(
            operation.getInterval().getStartMillis(), operation.getInterval().getEndMillis());
    boolean hasEqualInterval = intervalTree.contains(interval);
    if (!hasEqualInterval) return false;
    return intervalTree.query(interval).contains(new OperationToIntervalAdapter<>(operation));
  }

  @Override
  public Set<OperationTypeT> query(org.joda.time.Interval interval) {
    return Collections.unmodifiableSet(
        intervalTree.query(new LongInterval(interval.getStartMillis(), interval.getEndMillis()))
            .stream()
            .map(e -> ((OperationToIntervalAdapter<OperationTypeT>) e).operation)
            .collect(Collectors.toSet()));
  }

  @Override
  public Set<OperationTypeT> allOperations() {
    final Iterator<Interval<Long>> iterator = intervalTree.iterator();
    Set<OperationTypeT> operations = new HashSet<>();
    while (iterator.hasNext()) {
      operations.add(((OperationToIntervalAdapter<OperationTypeT>) iterator.next()).operation);
    }
    return Collections.unmodifiableSet(operations);
  }

  @Override
  public Iterator<OperationTypeT> iterator() {
    final Iterator<Interval<Long>> iterator = intervalTree.iterator();
    return new Iterator<OperationTypeT>() {
      @Override
      public boolean hasNext() {
        return iterator.hasNext();
      }

      @Override
      public OperationTypeT next() {
        return ((OperationToIntervalAdapter<OperationTypeT>) iterator.next()).operation;
      }
    };
  }

  @EqualsAndHashCode(of = "operation", callSuper = true)
  private static class OperationToIntervalAdapter<OperationTypeT extends IOperation<?>>
      extends LongInterval {

    private OperationTypeT operation;

    OperationToIntervalAdapter(OperationTypeT operation) {
      super(operation.getInterval().getStartMillis(), operation.getInterval().getEndMillis());
      this.operation = operation;
    }
  }
}
