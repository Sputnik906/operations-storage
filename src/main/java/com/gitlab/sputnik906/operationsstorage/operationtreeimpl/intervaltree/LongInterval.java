package com.gitlab.sputnik906.operationsstorage.operationtreeimpl.intervaltree;

import com.lodborg.intervaltree.Interval;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LongInterval extends Interval<Long> implements Comparable<LongInterval> {

  public LongInterval(Long start, Long end) {
    super(start, end, Interval.Bounded.OPEN);
  }

  public static LongInterval from(org.joda.time.Interval interval) {
    return new LongInterval(interval.getStartMillis(), interval.getEndMillis());
  }

  @Override
  protected LongInterval create() {
    return new LongInterval();
  }

  @Override
  public Long getMidpoint() {
    if (isEmpty()) return null;
    long from = getStart() == null ? Long.MIN_VALUE : getStart();
    long to = getEnd() == null ? Long.MAX_VALUE : getEnd();
    return ((from + to) / 2);
  }

  @Override
  public boolean isEmpty() {
    if (getStart() == null || getEnd() == null) return false;
    if (getStart() + 1 == getEnd() && !isEndInclusive() && !isStartInclusive()) return true;
    return super.isEmpty();
  }

  public int compareTo(LongInterval o) {
    return Long.compare(getStart(), o.getStart());
  }
}
