package com.gitlab.sputnik906.operationsstorage;

import java.util.Iterator;
import java.util.Set;
import org.joda.time.Interval;

public interface IOperationTree<ResourceT, OperationTypeT extends IOperation<ResourceT>> {
  boolean add(OperationTypeT operation);

  boolean remove(OperationTypeT operation);

  boolean contains(OperationTypeT operation);

  Set<OperationTypeT> query(Interval interval);

  Set<OperationTypeT> allOperations();

  Iterator<OperationTypeT> iterator();
}
